module.exports = function (app) {
  app.get("/", function (req, res) {
    // İSTEMCİNİN '/' İSTEĞİNE KARŞILIK VEREN KOD BLOĞU
    res.json({ message: `test deploy ${process.env.STRING_TEST}` });
    // res.render('index'); // INDEX VİEW DOSYASI RENDER EDİLDİ
  });
};
